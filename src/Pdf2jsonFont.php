<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

/**
 * Pdf2jsonFont class file.
 * 
 * This is a simple implementation of the Pdf2jsonFontInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class Pdf2jsonFont implements Pdf2jsonFontInterface
{
	
	/**
	 * The id of this font spec.
	 * 
	 * @var int
	 */
	protected int $_fontspec;
	
	/**
	 * The size of the font.
	 * 
	 * @var int
	 */
	protected int $_size;
	
	/**
	 * The family of the font.
	 * 
	 * @var string
	 */
	protected string $_family;
	
	/**
	 * The color of the font.
	 * @todo transform into html color code object
	 * 
	 * @var string
	 */
	protected string $_color;
	
	/**
	 * Constructor for Pdf2jsonFont with private members.
	 * 
	 * @param int $fontspec
	 * @param int $size
	 * @param string $family
	 * @param string $color
	 */
	public function __construct(int $fontspec, int $size, string $family, string $color)
	{
		$this->setFontspec($fontspec);
		$this->setSize($size);
		$this->setFamily($family);
		$this->setColor($color);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of this font spec.
	 * 
	 * @param int $fontspec
	 * @return Pdf2jsonFontInterface
	 */
	public function setFontspec(int $fontspec) : Pdf2jsonFontInterface
	{
		$this->_fontspec = $fontspec;
		
		return $this;
	}
	
	/**
	 * Gets the id of this font spec.
	 * 
	 * @return int
	 */
	public function getFontspec() : int
	{
		return $this->_fontspec;
	}
	
	/**
	 * Sets the size of the font.
	 * 
	 * @param int $size
	 * @return Pdf2jsonFontInterface
	 */
	public function setSize(int $size) : Pdf2jsonFontInterface
	{
		$this->_size = $size;
		
		return $this;
	}
	
	/**
	 * Gets the size of the font.
	 * 
	 * @return int
	 */
	public function getSize() : int
	{
		return $this->_size;
	}
	
	/**
	 * Sets the family of the font.
	 * 
	 * @param string $family
	 * @return Pdf2jsonFontInterface
	 */
	public function setFamily(string $family) : Pdf2jsonFontInterface
	{
		$this->_family = $family;
		
		return $this;
	}
	
	/**
	 * Gets the family of the font.
	 * 
	 * @return string
	 */
	public function getFamily() : string
	{
		return $this->_family;
	}
	
	/**
	 * Sets the color of the font.
	 * @todo transform into html color code object
	 * 
	 * @param string $color
	 * @return Pdf2jsonFontInterface
	 */
	public function setColor(string $color) : Pdf2jsonFontInterface
	{
		$this->_color = $color;
		
		return $this;
	}
	
	/**
	 * Gets the color of the font.
	 * @todo transform into html color code object
	 * 
	 * @return string
	 */
	public function getColor() : string
	{
		return $this->_color;
	}
	
}
