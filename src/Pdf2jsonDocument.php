<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

/**
 * Pdf2jsonDocument class file.
 * 
 * This is a simple implementation of the Pdf2jsonDocumentInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class Pdf2jsonDocument implements Pdf2jsonDocumentInterface
{
	
	/**
	 * The number of this page.
	 * 
	 * @var int
	 */
	protected int $_number;
	
	/**
	 * The quantity of pages.
	 * 
	 * @var int
	 */
	protected int $_pages;
	
	/**
	 * The height of the page.
	 * 
	 * @var int
	 */
	protected int $_height;
	
	/**
	 * The width of the page.
	 * 
	 * @var int
	 */
	protected int $_width;
	
	/**
	 * The fonts found in the document.
	 * 
	 * @var array<int, Pdf2jsonFontInterface>
	 */
	protected array $_fonts = [];
	
	/**
	 * The text snippets found in the document.
	 * 
	 * @var array<int, Pdf2jsonTextInterface>
	 */
	protected array $_text = [];
	
	/**
	 * Constructor for Pdf2jsonDocument with private members.
	 * 
	 * @param int $number
	 * @param int $pages
	 * @param int $height
	 * @param int $width
	 * @param array<int, Pdf2jsonFontInterface> $fonts
	 * @param array<int, Pdf2jsonTextInterface> $text
	 */
	public function __construct(int $number, int $pages, int $height, int $width, array $fonts, array $text)
	{
		$this->setNumber($number);
		$this->setPages($pages);
		$this->setHeight($height);
		$this->setWidth($width);
		$this->setFonts($fonts);
		$this->setText($text);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the number of this page.
	 * 
	 * @param int $number
	 * @return Pdf2jsonDocumentInterface
	 */
	public function setNumber(int $number) : Pdf2jsonDocumentInterface
	{
		$this->_number = $number;
		
		return $this;
	}
	
	/**
	 * Gets the number of this page.
	 * 
	 * @return int
	 */
	public function getNumber() : int
	{
		return $this->_number;
	}
	
	/**
	 * Sets the quantity of pages.
	 * 
	 * @param int $pages
	 * @return Pdf2jsonDocumentInterface
	 */
	public function setPages(int $pages) : Pdf2jsonDocumentInterface
	{
		$this->_pages = $pages;
		
		return $this;
	}
	
	/**
	 * Gets the quantity of pages.
	 * 
	 * @return int
	 */
	public function getPages() : int
	{
		return $this->_pages;
	}
	
	/**
	 * Sets the height of the page.
	 * 
	 * @param int $height
	 * @return Pdf2jsonDocumentInterface
	 */
	public function setHeight(int $height) : Pdf2jsonDocumentInterface
	{
		$this->_height = $height;
		
		return $this;
	}
	
	/**
	 * Gets the height of the page.
	 * 
	 * @return int
	 */
	public function getHeight() : int
	{
		return $this->_height;
	}
	
	/**
	 * Sets the width of the page.
	 * 
	 * @param int $width
	 * @return Pdf2jsonDocumentInterface
	 */
	public function setWidth(int $width) : Pdf2jsonDocumentInterface
	{
		$this->_width = $width;
		
		return $this;
	}
	
	/**
	 * Gets the width of the page.
	 * 
	 * @return int
	 */
	public function getWidth() : int
	{
		return $this->_width;
	}
	
	/**
	 * Sets the fonts found in the document.
	 * 
	 * @param array<int, Pdf2jsonFontInterface> $fonts
	 * @return Pdf2jsonDocumentInterface
	 */
	public function setFonts(array $fonts) : Pdf2jsonDocumentInterface
	{
		$this->_fonts = $fonts;
		
		return $this;
	}
	
	/**
	 * Gets the fonts found in the document.
	 * 
	 * @return array<int, Pdf2jsonFontInterface>
	 */
	public function getFonts() : array
	{
		return $this->_fonts;
	}
	
	/**
	 * Sets the text snippets found in the document.
	 * 
	 * @param array<int, Pdf2jsonTextInterface> $text
	 * @return Pdf2jsonDocumentInterface
	 */
	public function setText(array $text) : Pdf2jsonDocumentInterface
	{
		$this->_text = $text;
		
		return $this;
	}
	
	/**
	 * Gets the text snippets found in the document.
	 * 
	 * @return array<int, Pdf2jsonTextInterface>
	 */
	public function getText() : array
	{
		return $this->_text;
	}
	
}
