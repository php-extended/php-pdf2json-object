<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

use InvalidArgumentException;
use LogicException;
use PhpExtended\DataProvider\JsonFileDataProvider;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use RuntimeException;
use Throwable;

/**
 * Pdf2jsonExtractor class file.
 * 
 * This is a simple implementation of the Pdf2jsonExtractorInterface.
 * 
 * @author Anastaszor
 */
class Pdf2jsonExtractor implements Pdf2jsonExtractorInterface
{
	
	/**
	 * The reifier.
	 * 
	 * @var ?ReifierInterface
	 */
	protected ?ReifierInterface $_reifier = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Pdf2json\Pdf2jsonExtractorInterface::extractFromPdfFile()
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 */
	public function extractFromPdfFile(string $pdfFilePath, bool $useLocalJson = true, bool $eraseLocalJson = true) : Pdf2jsonDocument
	{
		if(!\is_file($pdfFilePath))
		{
			throw new InvalidArgumentException('No file found at '.$pdfFilePath);
		}
		if(!\is_readable($pdfFilePath))
		{
			throw new InvalidArgumentException('No readable file found at '.$pdfFilePath);
		}
		
		$statusCode = -1;
		/** @var array<integer, string> $output */
		$output = [];
		\exec('command -v pdf2json', $output, $statusCode);
		if(0 !== $statusCode || empty($output))
		{
			throw new LogicException('Failed to detect the pdf2json binary file, you should install it');
		}
		
		$jsonFilePath = \strpos($pdfFilePath, '.pdf') === false ? $pdfFilePath.'.json' : \str_replace('.pdf', '.json', $pdfFilePath);
		if(\is_file($jsonFilePath) && !$useLocalJson)
		{
			if(!@\unlink($jsonFilePath))
			{
				throw new RuntimeException('Failed to erase previous json file at '.$jsonFilePath);
			}
		}
		
		if(!\is_file($jsonFilePath))
		{
			$statusCode = -1;
			/** @var array<integer, string> $output */
			$output = [];
			\exec('pdf2json -enc UTF-8 '.\escapeshellarg($pdfFilePath).' '.\escapeshellarg($jsonFilePath), $output, $statusCode);
			if(0 !== $statusCode || empty($output))
			{
				/** @psalm-suppress MixedArgumentTypeCoercion */
				throw new RuntimeException('Failed to run pdf2json binary on pdf file at '.$pdfFilePath."\n".\implode("\n", $output));
			}
		}
		
		try
		{
			$json = new JsonFileDataProvider($jsonFilePath);
			
			$refied = $this->getReifier()->reifyAll(Pdf2jsonDocument::class, $json->provideAll());
			
			if(\count($refied) > 1)
			{
				throw new RuntimeException('Unexpected non-unique json document found in json file at '.$jsonFilePath);
			}
			
			if(\count($refied) < 1)
			{
				throw new RuntimeException('Unexpected empty json document found in json file at '.$jsonFilePath);
			}
			
			return \reset($refied);
		}
		catch(Throwable $exc)
		{
			throw new RuntimeException('Failed to handle the generated json file at '.$jsonFilePath, -1, $exc);
		}
		finally
		{
			if($eraseLocalJson)
			{
				@\unlink($jsonFilePath);
			}
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\Pdf2json\Pdf2jsonExtractorInterface::extractFromPdfString()
	 */
	public function extractFromPdfString(string $pdfString, ?string $tempDir = null) : Pdf2jsonDocument
	{
		$tempDir ??= '/tmp';
		$tempDir = \rtrim($tempDir, '/').'/';
		$fileName = \sha1($pdfString);
		$filePath = $tempDir.$fileName;
		
		$res = @\file_put_contents($filePath, $pdfString);
		if(false === $res)
		{
			throw new RuntimeException('Failed to write temporary data at file '.$filePath);
		}
		
		try
		{
			$document = $this->extractFromPdfFile($filePath, false, true);
		}
		catch(RuntimeException|LogicException|InvalidArgumentException $exc)
		{
			throw $exc;
		}
		finally
		{
			@\unlink($filePath);
		}
		
		return $document;
	}
	
	/**
	 * Gets the reifier.
	 * 
	 * @return ReifierInterface
	 */
	protected function getReifier() : ReifierInterface
	{
		if(null === $this->_reifier)
		{
			$this->_reifier = new Reifier();
			$this->_reifier->getConfiguration()->setIterableInnerType(Pdf2jsonDocument::class, 'fonts', Pdf2jsonFont::class);
			$this->_reifier->getConfiguration()->setIterableInnerType(Pdf2jsonDocument::class, 'text', Pdf2jsonText::class);
		}
		
		return $this->_reifier;
	}
	
}
