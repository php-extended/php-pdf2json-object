<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json;

/**
 * Pdf2jsonText class file.
 * 
 * This is a simple implementation of the Pdf2jsonTextInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class Pdf2jsonText implements Pdf2jsonTextInterface
{
	
	/**
	 * The absolute position from top border of the page of this text.
	 * 
	 * @var int
	 */
	protected int $_top;
	
	/**
	 * The absolute position from left border of the page of this text.
	 * 
	 * @var int
	 */
	protected int $_left;
	
	/**
	 * The width of this font snippet.
	 * 
	 * @var int
	 */
	protected int $_width;
	
	/**
	 * The height of this font snippet.
	 * 
	 * @var int
	 */
	protected int $_height;
	
	/**
	 * The font spec of this text snippet.
	 * 
	 * @var int
	 */
	protected int $_font;
	
	/**
	 * The text data found.
	 * 
	 * @var string
	 */
	protected string $_data;
	
	/**
	 * Constructor for Pdf2jsonText with private members.
	 * 
	 * @param int $top
	 * @param int $left
	 * @param int $width
	 * @param int $height
	 * @param int $font
	 * @param string $data
	 */
	public function __construct(int $top, int $left, int $width, int $height, int $font, string $data)
	{
		$this->setTop($top);
		$this->setLeft($left);
		$this->setWidth($width);
		$this->setHeight($height);
		$this->setFont($font);
		$this->setData($data);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the absolute position from top border of the page of this text.
	 * 
	 * @param int $top
	 * @return Pdf2jsonTextInterface
	 */
	public function setTop(int $top) : Pdf2jsonTextInterface
	{
		$this->_top = $top;
		
		return $this;
	}
	
	/**
	 * Gets the absolute position from top border of the page of this text.
	 * 
	 * @return int
	 */
	public function getTop() : int
	{
		return $this->_top;
	}
	
	/**
	 * Sets the absolute position from left border of the page of this text.
	 * 
	 * @param int $left
	 * @return Pdf2jsonTextInterface
	 */
	public function setLeft(int $left) : Pdf2jsonTextInterface
	{
		$this->_left = $left;
		
		return $this;
	}
	
	/**
	 * Gets the absolute position from left border of the page of this text.
	 * 
	 * @return int
	 */
	public function getLeft() : int
	{
		return $this->_left;
	}
	
	/**
	 * Sets the width of this font snippet.
	 * 
	 * @param int $width
	 * @return Pdf2jsonTextInterface
	 */
	public function setWidth(int $width) : Pdf2jsonTextInterface
	{
		$this->_width = $width;
		
		return $this;
	}
	
	/**
	 * Gets the width of this font snippet.
	 * 
	 * @return int
	 */
	public function getWidth() : int
	{
		return $this->_width;
	}
	
	/**
	 * Sets the height of this font snippet.
	 * 
	 * @param int $height
	 * @return Pdf2jsonTextInterface
	 */
	public function setHeight(int $height) : Pdf2jsonTextInterface
	{
		$this->_height = $height;
		
		return $this;
	}
	
	/**
	 * Gets the height of this font snippet.
	 * 
	 * @return int
	 */
	public function getHeight() : int
	{
		return $this->_height;
	}
	
	/**
	 * Sets the font spec of this text snippet.
	 * 
	 * @param int $font
	 * @return Pdf2jsonTextInterface
	 */
	public function setFont(int $font) : Pdf2jsonTextInterface
	{
		$this->_font = $font;
		
		return $this;
	}
	
	/**
	 * Gets the font spec of this text snippet.
	 * 
	 * @return int
	 */
	public function getFont() : int
	{
		return $this->_font;
	}
	
	/**
	 * Sets the text data found.
	 * 
	 * @param string $data
	 * @return Pdf2jsonTextInterface
	 */
	public function setData(string $data) : Pdf2jsonTextInterface
	{
		$this->_data = $data;
		
		return $this;
	}
	
	/**
	 * Gets the text data found.
	 * 
	 * @return string
	 */
	public function getData() : string
	{
		return $this->_data;
	}
	
}
