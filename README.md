# php-extended/php-pdf2json-object

An implementation of the php-extended/php-pdf2json-interface library.

![coverage](https://gitlab.com/php-extended/php-pdf2json-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-pdf2json-object/badges/master/coverage.svg?style=flat-square)

This library was made to get text positions accurate while there are positional
calculus errors or no implementations from the
[smalot/pdfparser](https://github.com/smalot/pdfparser) library.


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-pdf2json-object ^8`

`/!\`

This library REQUIRES the installation of the pdf2json library as native library.
[More instructions on their github page.](https://github.com/flexpaper/pdf2json)

This library does not support OSes others than linux for the moment.


## Basic Usage

This library can be used the following way :

```php

use PhpExtended\Pdf2json\Pdf2jsonExtractor;

$extractor = new Pdf2jsonExtractor();

$document = $extractor->extractFromPdfFile('<path/to/pdf/document.pdf>');

foreach($document->text as $text)
{
	/** @var $text \PhpExtended\Pdf2json\Pdf2jsonText */
}

```


## License

MIT (See [license file](LICENSE)).
