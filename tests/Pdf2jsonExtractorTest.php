<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\Pdf2json\Pdf2jsonExtractor;
use PhpExtended\Pdf2json\Pdf2jsonText;
use PHPUnit\Framework\TestCase;

/**
 * Pdf2jsonExtractorTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Pdf2json\Pdf2jsonExtractor
 * @internal
 * @small
 */
class Pdf2jsonExtractorTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Pdf2jsonExtractor
	 */
	protected Pdf2jsonExtractor $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testItWorks() : void
	{
		if(!\is_dir('/media/anastaszor/WORKSPACE'))
		{
			$this->markTestSkipped('pdf2json binary is not installed on CI');
		}
		
		$document = $this->_object->extractFromPdfFile(__DIR__.'/sample.pdf', true, true);
		
		$text = [];
		
		/** @var Pdf2jsonText $textElement */
		foreach($document->getText() as $textElement)
		{
			$text[] = $textElement->getData();
		}
		
		$this->assertEquals('This is a sample text document.', \implode('', $text));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Pdf2jsonExtractor();
	}
	
}
