<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json\Test;

use PhpExtended\Pdf2json\Pdf2jsonText;
use PHPUnit\Framework\TestCase;

/**
 * Pdf2jsonTextTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Pdf2json\Pdf2jsonText
 * @internal
 * @small
 */
class Pdf2jsonTextTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Pdf2jsonText
	 */
	protected Pdf2jsonText $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetTop() : void
	{
		$this->assertEquals(12, $this->_object->getTop());
		$expected = 25;
		$this->_object->setTop($expected);
		$this->assertEquals($expected, $this->_object->getTop());
	}
	
	public function testGetLeft() : void
	{
		$this->assertEquals(12, $this->_object->getLeft());
		$expected = 25;
		$this->_object->setLeft($expected);
		$this->assertEquals($expected, $this->_object->getLeft());
	}
	
	public function testGetWidth() : void
	{
		$this->assertEquals(12, $this->_object->getWidth());
		$expected = 25;
		$this->_object->setWidth($expected);
		$this->assertEquals($expected, $this->_object->getWidth());
	}
	
	public function testGetHeight() : void
	{
		$this->assertEquals(12, $this->_object->getHeight());
		$expected = 25;
		$this->_object->setHeight($expected);
		$this->assertEquals($expected, $this->_object->getHeight());
	}
	
	public function testGetFont() : void
	{
		$this->assertEquals(12, $this->_object->getFont());
		$expected = 25;
		$this->_object->setFont($expected);
		$this->assertEquals($expected, $this->_object->getFont());
	}
	
	public function testGetData() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getData());
		$expected = 'qsdfghjklm';
		$this->_object->setData($expected);
		$this->assertEquals($expected, $this->_object->getData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Pdf2jsonText(12, 12, 12, 12, 12, 'azertyuiop');
	}
	
}
