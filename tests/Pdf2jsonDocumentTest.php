<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json\Test;

use PhpExtended\Pdf2json\Pdf2jsonDocument;
use PhpExtended\Pdf2json\Pdf2jsonFont;
use PhpExtended\Pdf2json\Pdf2jsonText;
use PHPUnit\Framework\TestCase;

/**
 * Pdf2jsonDocumentTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Pdf2json\Pdf2jsonDocument
 * @internal
 * @small
 */
class Pdf2jsonDocumentTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Pdf2jsonDocument
	 */
	protected Pdf2jsonDocument $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetNumber() : void
	{
		$this->assertEquals(12, $this->_object->getNumber());
		$expected = 25;
		$this->_object->setNumber($expected);
		$this->assertEquals($expected, $this->_object->getNumber());
	}
	
	public function testGetPages() : void
	{
		$this->assertEquals(12, $this->_object->getPages());
		$expected = 25;
		$this->_object->setPages($expected);
		$this->assertEquals($expected, $this->_object->getPages());
	}
	
	public function testGetHeight() : void
	{
		$this->assertEquals(12, $this->_object->getHeight());
		$expected = 25;
		$this->_object->setHeight($expected);
		$this->assertEquals($expected, $this->_object->getHeight());
	}
	
	public function testGetWidth() : void
	{
		$this->assertEquals(12, $this->_object->getWidth());
		$expected = 25;
		$this->_object->setWidth($expected);
		$this->assertEquals($expected, $this->_object->getWidth());
	}
	
	public function testGetFonts() : void
	{
		$this->assertEquals([$this->getMockBuilder(Pdf2jsonFont::class)->disableOriginalConstructor()->getMock()], $this->_object->getFonts());
		$expected = [$this->getMockBuilder(Pdf2jsonFont::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(Pdf2jsonFont::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setFonts($expected);
		$this->assertEquals($expected, $this->_object->getFonts());
	}
	
	public function testGetText() : void
	{
		$this->assertEquals([$this->getMockBuilder(Pdf2jsonText::class)->disableOriginalConstructor()->getMock()], $this->_object->getText());
		$expected = [$this->getMockBuilder(Pdf2jsonText::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(Pdf2jsonText::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setText($expected);
		$this->assertEquals($expected, $this->_object->getText());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Pdf2jsonDocument(12, 12, 12, 12, [$this->getMockBuilder(Pdf2jsonFont::class)->disableOriginalConstructor()->getMock()], [$this->getMockBuilder(Pdf2jsonText::class)->disableOriginalConstructor()->getMock()]);
	}
	
}
