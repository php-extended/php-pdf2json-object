<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-pdf2json-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Pdf2json\Test;

use PhpExtended\Pdf2json\Pdf2jsonFont;
use PHPUnit\Framework\TestCase;

/**
 * Pdf2jsonFontTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\Pdf2json\Pdf2jsonFont
 * @internal
 * @small
 */
class Pdf2jsonFontTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Pdf2jsonFont
	 */
	protected Pdf2jsonFont $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetFontspec() : void
	{
		$this->assertEquals(12, $this->_object->getFontspec());
		$expected = 25;
		$this->_object->setFontspec($expected);
		$this->assertEquals($expected, $this->_object->getFontspec());
	}
	
	public function testGetSize() : void
	{
		$this->assertEquals(12, $this->_object->getSize());
		$expected = 25;
		$this->_object->setSize($expected);
		$this->assertEquals($expected, $this->_object->getSize());
	}
	
	public function testGetFamily() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFamily());
		$expected = 'qsdfghjklm';
		$this->_object->setFamily($expected);
		$this->assertEquals($expected, $this->_object->getFamily());
	}
	
	public function testGetColor() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getColor());
		$expected = 'qsdfghjklm';
		$this->_object->setColor($expected);
		$this->assertEquals($expected, $this->_object->getColor());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new Pdf2jsonFont(12, 12, 'azertyuiop', 'azertyuiop');
	}
	
}
